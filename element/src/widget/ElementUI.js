import declare from 'dojoBaseDeclare';
import widgetBase from 'widgetBase';
import _TemplatedMixin from 'dijitTemplatedMixin';
import template from './template/template.html'

let Vue = window.Vue;
import App from './App.vue';

declare("ElementUI.widget.ElementUI", [widgetBase, _TemplatedMixin], {
    templateString: template,

    vueRoot: null,
    contextObj: null,
    constructor: function (params, srcNodeRef) {

    },

    update: function (obj, cb) {
        this.contextObj = obj;
        let vm = this;
        // 从环境中取国际化信息
        let code = mx.session.sessionData.locale.code;
        console.log(code);
        if ("zh_CN" === code) {
            ELEMENT.locale(ELEMENT.lang.zhCN)
        } else  {
            ELEMENT.locale(ELEMENT.lang.en)
        };

        if (!this.vueRoot) {//防止页面复用问题
            this.vueRoot = new Vue({
                el: this.mxVueWidget,
                render: h => h(App, {
                    props: {
                        widgetID: this.id
                    }
                })
            });
        }
// Emit the event that will send the data from Mendix to your App.vue file / app starting point.
        this.vueRoot.$emit('widget-loaded', {
            btnName: vm.btnName
        });

        if (this.contextObj != null) {
            var subscription = mx.data.subscribe({
                guid: this.contextObj.getGuid(),
                callback: function (guid) {
                    vm.vueRoot.$emit("widget-change");
                    console.log("Object with guid " + guid + " changed");
                }
            });
        }
        cb();
    },
})
;

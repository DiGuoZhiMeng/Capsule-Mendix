## Capsule-Mendix是基于国内各[知名Vue UI组件库](https://www.jianshu.com/p/398a3b2e535f/)开发的Mendix UI系列组件
> [View UI](https://www.iviewui.com/docs/introduce)

> [Ant Design of Vue](https://www.antdv.com/docs/vue/introduce-cn/)

> [Element UI组件库](https://element.eleme.cn/#/zh-CN/)

> [Vant移动端组件库](https://youzan.github.io/vant/#/zh-CN/)

#### [GITHUB](https://github.com/DiGuoZhiMeng/Capsule-Mendix) | [GITEE](https://gitee.com/DiGuoZhiMeng/Capsule-Mendix)

### 主要特性
- 实现按需加载组件，减少文件体积
- 抽取Vue引擎代码、UI组件css及字体文件、polyfill等公共代码至mendix全局index.html中，最大程度减少组件体积，以view-design为例，目前50余个组件放到一个组件包中，大小在130KB左右，平均每个组件在几KB大小
- 支持所有的现代浏览器和 IE10+
- 支持组件自身实现的国际化，也支持Vue i18n

### 项目环境 
工具软件 | 版本 |  备注
-|-|-
Git | 2.15.0 |  |
NPM | 6.4.1 |  |
Mendix | 7.14.1 | [下载地址](https://appstore.home.mendix.com/link/modelers/) |

### 技术选型 
技术 | 版本 |  备注
-|-|-
Vue | 2.6.8 | 基于Vue 2.x |
Babel| 7.10.3 | 语法转换器 |
Webpack | 4.29.6 | 打包工具 |
view-design | 4.2.0 | iView |
element-ui | 2.13.2 |  |
Ant Design Vue | 1.6.2 | antd |

## 快速开始
### 克隆 Capsule-Mendix
```bash
git clone https://gitee.com/DiGuoZhiMeng/Capsule-Mendix
cd Capsule-Mendix
```
### 安装zip压缩工具
> 将仓库根目录下zip.exe和bzip2.dll复制到自己Git安装目录下的usr/bin文件夹中 

### 初始化环境
> 进入组件文件夹中，在Git bash中执行如下命令： 

```bash
npm install
```
> 进入组件文件夹下demo_project文件夹中，双击Demo.mpr文件运行示例工程，打开后Run Local

> Mendix示例工程启动后，在Git bash中执行如下命令： 

```bash
npm run build
```

> 如果需要进行生成环境打包，需要更改webpack.config.js中如下位置代码，将development改为production 

```bash
mode: "development", // Change the mode to "production" before you go live! Don't forget!
plugins: [
    new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: '"development"' // change to "production" when publishing your Mendix widget
         }
    }),
```

> 如果需要后台自动监控即时打包，不需要每次更改都手动执行npm run build，则只需要在Git bash中执行如下命令，每次更新代码，都会自动打包，并把新的mpk文件部署到mendix工程中，在页面上打开F12的前提下，可以清空缓存并强制硬性加载，即可看到最新组件效果。 

```bash
npm run watch
```
## View Design整合思路

> view design\demo_project\theme\index.html中需要全局引入内容

```html
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Mendix</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css?{{cachebust}}">
        <link rel="stylesheet" href="mxclientsystem/mxui/ui/mxui.css?{{cachebust}}">
        <link rel="stylesheet" href="css/theme.css?{{cachebust}}">
        <!--全局引入iview样式代码-->
        <link rel="stylesheet" href="iview/iview.css?{{cachebust}}">
    </head>
    <body dir="ltr">
        <div id="content"></div>
        <!--全局引入vue引擎代码-->
        <script src="vue/vue.min.js?{{cachebust}}"></script>
        <!--全局引入polyfill兼容IE 10-->
        <script src="polyfill/polyfill.min.js?{{cachebust}}"></script>
        <script>
            dojoConfig = {
                baseUrl: "mxclientsystem/dojo/",
                cacheBust: "{{cachebust}}",
                rtlRedirect: "index-rtl.html"
            };
        </script>
        <script src="mxclientsystem/mxui/mxui.js?{{cachebust}}"></script>

    </body>
</html>

```

> view design\src\widget\ViewDesign.js中不再需要全局引入Vue，按需引入国际化的语言配置，在update回调方法中动态设置国际化信息

```javascript
// 国际化按需引入，兼容vue-i18n的方式参考：https://www.iviewui.com/docs/guide/i18n
import {locale} from 'view-design';
import en from 'view-design/dist/locale/en-US';
import zh from 'view-design/dist/locale/zh-CN';

// 从环境中取国际化信息
let code = mx.session.sessionData.locale.code;
console.log(code);
if ("zh_CN" === code) {
    locale(zh);
} else if ("en_US" === code) {
    locale(en);
}

```
> webpack.config.js文件中：externals需要增加"vue"排除项的设置，babel-loader的规则中增加解析node_modules/view-design/src

```javascript
{
    test: /\.js$/,
    loader: 'babel-loader',
    include: [path.resolve(__dirname, 'src'), path.resolve(__dirname, 'node_modules/view-design/src')]
},

externals: {
    vue: 'vue/vue.min.js', // 排除vue依赖，减少每个组件的体积，统一在index.html中引入
    dojoBaseDeclare: "dojo/_base/declare",
    widgetBase: "mxui/widget/_WidgetBase",
    dijitTemplatedMixin: "dijit/_TemplatedMixin",
},
```
> babel.config.js中配置按需引入

```javascript
module.exports = {
  presets: [
    [
      "@babel/preset-env",
      {
        targets: {
          browsers: ["last 3 versions", "ie>=9"]
        },
        debug: false
      }
    ]
  ],
  plugins: [
    "@babel/plugin-transform-runtime",
    "@babel/plugin-syntax-dynamic-import",
    "@babel/plugin-proposal-object-rest-spread",
    "transform-vue-jsx",
    [
      "import",
      {
        libraryName: "view-design",
        libraryDirectory: "src/components"
      }
    ]
  ]
};

```

## Element UI整合思路
> element\demo_project\theme\index.html中需要全局引入内容

```html
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Mendix</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css?{{cachebust}}">
        <link rel="stylesheet" href="mxclientsystem/mxui/ui/mxui.css?{{cachebust}}">
        <link rel="stylesheet" href="css/theme.css?{{cachebust}}">
        <!--引入element UI全局样式-->
        <link rel="stylesheet" href="element/element.min.css?{{cachebust}}">
    </head>
    <body dir="ltr">
        <div id="content"></div>
        <!--polyfill支持IE 10-->
        <script src="polyfill/polyfill.min.js?{{cachebust}}"></script>
        <!--全局引入vue-->
        <script src="vue/vue.min.js?{{cachebust}}"></script>
        <!--全局引入element.js-->
		<script src="element/element.min.js?{{cachebust}}"></script>
        <!--全局引入国际化语言：中文和英文-->
        <script src="element/locale/en.js?{{cachebust}}"></script>
        <script src="element/locale/zh-CN.js?{{cachebust}}"></script>
        <script>
            dojoConfig = {
                baseUrl: "mxclientsystem/dojo/",
                cacheBust: "{{cachebust}}",
                rtlRedirect: "index-rtl.html"
            };
        </script>
        <script src="mxclientsystem/mxui/mxui.js?{{cachebust}}"></script>
    </body>
</html>
```
> element\src\widget\ElementUI.js中中不再需要全局引入Vue，不需要引入Element的国际化任何内容，但是要在update回调方法中动态设置ELEMENT国际化信息

```javascript
// 从环境中取国际化信息
let code = mx.session.sessionData.locale.code;
if ("zh_CN" === code) {
    ELEMENT.locale(ELEMENT.lang.zhCN)
} else  {
    ELEMENT.locale(ELEMENT.lang.en)
};
```
> webpack.config.js文件中module.rule,babel-loader不需要include node_module/element-ui/

```javascript
{
    test: /\.js$/,
    loader: 'babel-loader',
    include: [path.resolve(__dirname, 'src')]
},
```
> webpack.config.js文件中externals不需要增加"element-ui"排除项的设置

 ```javascript
externals: {
    dojoBaseDeclare: "dojo/_base/declare",
    widgetBase: "mxui/widget/_WidgetBase",
    dijitTemplatedMixin: "dijit/_TemplatedMixin",
},
```
> babel.config.js中不需要按需引入的配置

```javascript
module.exports = {
    presets: [
        [
            "@babel/preset-env",
            {
                targets: {
                    browsers: ["last 3 versions", "ie>=9"]
                },
                debug: false
            }
        ]
    ],
    plugins: [
        "@babel/plugin-transform-runtime",
        "@babel/plugin-syntax-dynamic-import",
        "@babel/plugin-proposal-object-rest-spread",
        "transform-vue-jsx",
    ]
};

```

## Ant Design整合思路
> ant design\demo_project\theme\index.html中需要全局引入内容

```html
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Mendix</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css?{{cachebust}}">
        <link rel="stylesheet" href="mxclientsystem/mxui/ui/mxui.css?{{cachebust}}">
        <link rel="stylesheet" href="css/theme.css?{{cachebust}}">
        <!--全局引入And Design样式表-->
        <link rel="stylesheet" href="antd/antd.min.css?{{cachebust}}">

    </head>
    <body dir="ltr">
        <div id="content"></div>
        <!--polyfill支持IE 10-->
        <script src="polyfill/polyfill.min.js?{{cachebust}}"></script>
        <!--全局引入vue-->
        <script src="vue/vue.min.js?{{cachebust}}"></script>
        <!--全局引入moment, antd日期时间组件依赖-->
        <script src="antd/moment.min.js?{{cachebust}}"></script>
        <!--全局引入antd.js，带国际化信息-->
        <script src="antd/antd-with-locales.min.js?{{cachebust}}"></script>
        <script>
            dojoConfig = {
                baseUrl: "mxclientsystem/dojo/",
                cacheBust: "{{cachebust}}",
                rtlRedirect: "index-rtl.html"
            };
        </script>
        <script src="mxclientsystem/mxui/mxui.js?{{cachebust}}"></script>

    </body>
</html>
```
> ant design\src\widget\AntDesign.js中不再需要全局引入Vue，不需要引入Ant Design的国际化任何内容，但是要在App.vue中外层包LocaleProvider组件，然后locale再动态设置国际化信息

```javascript
<template>
    <a-locale-provider :locale="locale">
        
    </a-locale-provider>
</template>
const {locales} = window.antd;
locale: this.getLocale(),
getLocale() {
    // 从环境中取国际化信息
    let code = mx.session.sessionData.locale.code;
    console.log(code);
    if ("zh_CN" === code) {
        return locales.zh_CN;
    } else {
        return locales.en_US;
    }
}
```
> webpack.config.js文件中module.rule,babel-loader不需要include node_module/ant-design-vue/

```javascript
{
test: /\.js$/,
loader: 'babel-loader',
    include: [path.resolve(__dirname, 'src')]
},
```
> webpack.config.js文件中externals不需要增加"ant-design-vue"排除项的设置

```javascript
externals: {
    dojoBaseDeclare: "dojo/_base/declare",
    widgetBase: "mxui/widget/_WidgetBase",
    dijitTemplatedMixin: "dijit/_TemplatedMixin",
},
```
> babel.config.js中不需要按需引入的配置

```javascript
module.exports = {
    presets: [
        [
            "@babel/preset-env",
            {
                targets: {
                    browsers: ["last 3 versions", "ie>=9"]
                },
                debug: false
            }
        ]
    ],
    plugins: [
        "@babel/plugin-transform-runtime",
        "@babel/plugin-syntax-dynamic-import",
        "@babel/plugin-proposal-object-rest-spread",
        "transform-vue-jsx",
    ]
};

```

## 特别鸣谢
> 本代码库的开发模板主要参考了高先生的[mendix_vue_template](https://github.com/MrGaoGang/mendix_vue_template)
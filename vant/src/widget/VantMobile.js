import declare from 'dojoBaseDeclare';
import widgetBase from 'widgetBase';
import _TemplatedMixin from 'dijitTemplatedMixin';
import template from './template/template.html'
import App from './App.vue';
// 如果当前组件涉及国际化，则按需引入即可
// import {Locale} from 'vant';
// import enUS from 'vant/lib/locale/lang/en-US';
// import zhCN from 'vant/lib/locale/lang/zh-CN';

let Vue = window.Vue;

declare("VantMobile.widget.VantMobile", [widgetBase, _TemplatedMixin], {
    templateString: template,

    vueRoot: null,
    contextObj: null,
    constructor: function (params, srcNodeRef) {

    },

    update: function (obj, cb) {
        this.contextObj = obj;
        let vm = this;

        // 从环境中取国际化信息
        // let code = mx.session.sessionData.locale.code;
        // console.log(code);
        // if ("zh_CN" === code) {
        //     Locale.use('zh-CN', zhCN);
        // } else if ("en_US" === code) {
        //     Locale.use('en-US', enUS);
        // }

        if (!this.vueRoot) {//防止页面复用问题
            this.vueRoot = new Vue({
                el: this.mxVueWidget,
                render: h => h(App, {
                    props: {
                        widgetID: this.id
                    }
                })
            });
        }
// Emit the event that will send the data from Mendix to your App.vue file / app starting point.
        this.vueRoot.$emit('widget-loaded', {
            btnName: vm.btnName
        });

        if (this.contextObj != null) {
            var subscription = mx.data.subscribe({
                guid: this.contextObj.getGuid(),
                callback: function (guid) {
                    vm.vueRoot.$emit("widget-change");
                    console.log("Object with guid " + guid + " changed");
                }
            });
        }
        cb();
    },
})
;
